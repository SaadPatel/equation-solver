#ifndef TASK3_H
#define TASK3_H

#include<string>
#include<iostream>
#include "Task1.h"

using namespace std;

class Task3 :public Task1
{   
    private:
    string error;

    protected:
    vector <double> solve();

    public:
    bool check(string e);
    string output();

};

#endif