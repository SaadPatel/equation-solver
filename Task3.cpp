// 

#include "Task3.h"

bool Task3::check(string e)
{
    int equal = e.find("=");      
    
    if(equal == string::npos)
        return false;
    
    int firstx = e.find("x");

    if(firstx == string::npos || firstx > equal)     //these 2 conditions check for x on either side of =
        return false;

    int secondx = e.find("x", equal);
    
    if(secondx == string::npos || secondx < equal)
        return false;
    
    if(e.find("y") != string::npos || e.find("z") != string::npos)      // eliminate all other cases
        return false;
    
    if(e.find("^") != string::npos)
        return false;
    
    if(e.find("cos")!= string::npos || e.find("sin")!= string::npos)
        return false;
    
    if(e.find("|")!= string::npos)
        return false;
    
    if(e.find("ln") != string::npos)
        return false;
    
    return true;                         
}

vector <double> Task3::solve()
{
    int equal = equation.find("=");
    
    string lhs = equation.substr(0,equal);                          //divide equation into rhs and lhs
    string rhs = equation.substr(equal + 1, equation.size());


    string lhsOperation;

    float lhsXcoeff;
    float lhsConstant;

    if(lhs.find("-") != string::npos && lhs.find("+") != string::npos) //handles the special case -x+b
    {
        lhsOperation = "+";
    }
    else if(lhs.find("+") != string::npos)              // finds out if operation is - or +
    {
        lhsOperation = "+";
    }
    else if(lhs.find("-") != string::npos)
    {
        lhsOperation = "-";
    }
    else                                    //for the cases like 2x=3x+2
    {
        lhs += "+0";
        lhsOperation = "+";
    }

    if(lhs.find("x") < lhs.find(lhsOperation))  //for case ax+b
    {   
        if(lhs.find("x") == 1 and lhs[0] == '-' )   //for special case -x+b
        {
            lhsXcoeff = -1;
        }
        else if(lhs.find("x") != 0)
        {
            lhsXcoeff = stof(lhs);
        }
        else                    //for the case x+b
        {
            lhsXcoeff = 1;
        }
        lhsConstant = stof(lhs.substr(lhs.find(lhsOperation), lhs.size()));
    }
    else            //for the case b+ax
    {   
        if(lhs.find("x") == lhs.find(lhsOperation) + 1)
        {
            lhsXcoeff = stof(lhs.substr(lhs.find(lhsOperation), lhs.size()));
        }
        else
        {
            lhsXcoeff = 1;
        }
        
        lhsConstant = stof(lhs);
    }

    string rhsOperation;        //similar procedure for rhs

    float rhsXcoeff;
    float rhsConstant;
    
    if(rhs.find("-") != string::npos && rhs.find("+") != string::npos)
    {
        rhsOperation = "+";
    }
    if(rhs.find("+") != string::npos)
    {
        rhsOperation = "+";
    }
    else if(rhs.find("-") != string::npos)
    {
        rhsOperation = "-";
    }
    else
    {
        rhs += "+0";
        rhsOperation = "+";
    }

    if(rhs.find("x") < rhs.find(rhsOperation))
    {
        if(rhs.find("x") == 1 and rhs[0] == '-' )
        {
            rhsXcoeff = -1;
        }
        else if(rhs.find("x") != 0)
        {
            rhsXcoeff = stof(rhs);
        }
        else
        {
            rhsXcoeff = 1;
        }
        rhsConstant = stof(rhs.substr(rhs.find(rhsOperation), rhs.size()));
    }
    else
    {
        if(rhs.find("x") == rhs.find(rhsOperation) + 1)
        {
            rhsXcoeff = stof(rhs.substr(rhs.find(rhsOperation), rhs.size()));
        }
        else
        {
            rhsXcoeff = 1;
        }
        rhsConstant = stof(rhs);
    }

    equation = "";              //create new equation to pass to task1
    if(lhsXcoeff == rhsXcoeff)          //check for infinite or no solutions
    {
        if(lhsConstant == rhsConstant)
        {
            error = "Infinite Solutions";
        }
        else
        {
            error = "no solutions";
        }
        
    }
    else
    {
        error = "";
    }
    

    equation += to_string(lhsXcoeff - rhsXcoeff);
    equation += "x";
    
    if(lhsOperation == "+")
        equation += lhsOperation;
    
    equation += to_string(lhsConstant);
    equation += "=";
    equation += to_string(rhsConstant);
    vector <double> x = Task1::solve();    //calling task1 to solve the converted equation (a-c)x+b=d
    return x;   
}

string Task3::output()
{
    string out;
    vector <double> x = solve();
    char varx[20];
    sprintf(varx,"%.4lf",x[0]);     //to round of to 4 decimal places.
    string var(varx);
    if(error == "")
        out = "x=" + var + ";";
    else
        out = error;
    return out;
}