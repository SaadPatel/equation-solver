#include<bits/stdc++.h>
#include"Task1.h"


using namespace std;

bool Task1::check(string s1){
    if(s1.find('y')== string::npos && s1.find('z')== string::npos &&  s1.find('^')== string::npos && s1.find("sin")== string::npos && s1.find("cos")== string::npos){
        if(s1.find('x')!= string::npos){
            s1.erase(s1.find('x'),1);
            //cout<<"p"<<s1;
            if(s1.find('x')!= string::npos)
                return false;
            else
                return true;            
        }
    }
    
    return false;

}
string Task1::output(){
    solve();
    return ans;
}

vector <double> Task1::solve(){
    string s1 = equation;
    // cin >> s1;

    double p=0,q=0,r=0,l=0,u=0,v=0,w=0,f=0;
    l = s1.size();
// p q r as + = x
    // if(s1.find("+") == -1){
    //     s1="0+"+s1;
    // }
    for(int i=0;i<s1.length(); i++){
        if(s1.at(i)=='x'){
            r=i;
        }
        else if((s1.at(i)=='+' || s1.at(i)=='-') && i!=0 && f==0){
            if(q!=0 ){
                p = i;
            }
            else{
                    p=i;
                    f=1;   
                }
        }
        else if(s1.at(i)=='='){
            q=i;
        }
    }
         //cout<<p<<endl;
         //cout<<q<<endl;
         //cout<<r<<endl;


    if(p<r && r<q){
        // p r q : b+ax=c :- ux+v=w p q r as + = x

        string s2(s1,p,r-p); //r-1 -p-1 +1
        string s3(s1,0,p);
        string s4(s1,q+1,l-q+1); // l-1 - q-1 +1
        
        if(s2 == "-" && s1.find('x')!=-1)
            u =-1;
        else if( s2 == ""&& s1.find('x')!=-1)
            u=1;
        else
         u = stof(s2);

        if(s3 == ""){
            v = 0;
        }else
        v = stof(s3);
        
        w = stof(s4);

         //cout<<s2<<endl;
         //cout<<s3<<endl;

         //cout<<"d"<<endl;

    }
    else if(q<p && p<r){
        // q p r  : c=b+ax :- ux+v=w p q r as + = x

        string s2(s1,p,r-p);
        string s3(s1,q+1,p-1-q);
        string s4(s1,0,q);
        
        if(s2 == "-" && s1.find('x')!=-1)
            u =-1;
        else if( s2 == ""&& s1.find('x')!=-1)
            u=1;
        else
         u = stof(s2);
        v= stof(s3);
        w = stof(s4);
         //cout<<s2<<endl;
         //cout<<"c"<<endl;

        
    }
    else if(q<r && r<p){
        // q r p : c=ax+b :- ux+v=w p q r as + = x
        // c=-ax-b

        string s2(s1,q+1,r-1-q);
        string s3(s1,r+1,l-r-1);
        string s4(s1,0,q);
        
         if(s2 == "-" && s1.find('x')!=-1)
            u =-1;
        else if( s2 == ""&& s1.find('x')!=-1)
            u=1;
        else
         u = stof(s2);
         v= stof(s3);
          w = stof(s4);
         //cout<<s2<<endl;
         //cout<<s3<<endl;
         //cout<<s4<<endl;
         //cout<<"b"<<endl;

        
    }
    else if(r<p && p<q){
        // r p q: ax+b=c :- ux+v=w p q r as + = x
    //cout<<"p q r are "<<p<<q<<r<<endl;
        string s2(s1,0,r);
        string s3(s1,p,q-p);
        string s4(s1,q+1,l-q);

        //cout<<s2<<endl;
         //cout<<s3<<endl;
         //cout<<s4<<endl;
        
        if(s2 == "-" && s1.find('x')!=-1)
            u =-1;
        else if( s2 == ""&& s1.find('x')!=-1)
            u=1;
        else
         u = stof(s2);
         
         v= stof(s3);
         w = stof(s4);

         //cout<<s2<<endl;
         //cout<<s3<<endl;
         //cout<<s4<<endl;
         //cout<<u<<endl;
         //cout<<v<<endl;
         //cout<<w<<endl;
         //cout<<"a"<<endl;

    }
    
    
    double n = (w-v)/u;


    //cout<<n;


    vector <double> x;
    x.push_back(n);
    char varx[20];
    sprintf(varx,"%.4lf", n);
    string var(varx);


    ans= "x=" + var;
    return x;



}


