#ifndef CALCULATOR_H
#define CALCULATOR_H

#include "TaskTemplate.h"
#include "Task1.h"
#include "Task2.h"
#include "Task3.h"
#include "Task4.h"
#include "Task5.hpp"
#include "Task6.h"
#include <vector>
#include<memory>

using namespace std;


class Calculator
{
    private:
    vector <unique_ptr <TaskInterface> > tasks;

    public:
    Calculator();

    void calculate(string e);
};

#endif