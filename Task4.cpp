// 

#include"Task4.h"
#include<utility>
#include<math.h>
#include<string>
#include<bits/stdc++.h>


bool Taskfour::check(string e)
    {
    int i;
    for(i=0;i<e.size();i++)
        {
        if(e[i]=='^')
            {
            return true;
            }
        }
    return false;
    } 

string Taskfour::output()
    {
    this->solve();    
    string final_output;
    char varx[10];
    sprintf(varx,"%.4lf",roots[0]);
    string var1(varx);
    sprintf(varx,"%.4lf",roots[1]);
    string var2(varx);
    if(solution_exists==true)
        {
        final_output="x=";
        final_output.append(var1);
        final_output.append(";x=");
        final_output.append(var2);
        
        }  
    else
        {
        final_output="No Solution";
        }
    return final_output;
    }

vector <double> Taskfour::solve()
    {    
    int flag1,flag2,i,flag3=1;
    float root1,root2,a,b,c,d;
    a=1;
    b=0;
    c=0;
    d=0;
    root1=0;
    root2=0;
    flag1=0;
    flag2=0;
    float discriminant;    
    char* input=new char[90];
    input=&*equation.begin();
    char* token ;
    if(equation[0]!='x')
        {
        token = strtok(input, "x^");
        if(token!=NULL)
            {
            a = strtod(token, NULL);
            }
        }

   
    for(i=0;i<equation.size()-1;i++)
        {
        if(equation[i]=='x' && equation[i+1]!='^' && (equation[i-1]=='-' || equation[i-1]=='+') )
            {
            flag3=0;
            if(equation[i-1]=='+')
                {
                b=1;
                }
            else
                {
                b=-1;
                }
            }
        if(equation[i]=='x' && equation[i+1]!='^')
            {
            flag1=1;
            }
        if(equation[i]>=48 && equation[i]<=56 && equation[i+1]=='=')
            {
            flag2=1;
            }
        }
    // if  the first sign is plus
    // token = strtok(NULL, "+");
    if(equation[0]=='x')
        {
        token =strtok(input,"2");
        }
    else
        {
         token = strtok(NULL, "2");
        }
    if(flag1==1)
       {
        token = strtok(NULL, "x");
        if(flag3==1)
            { 
            b = strtod(token, NULL);
            }
        }
    
    // if the second sign is also plus
    if(flag2==1)
        {
        token = strtok(NULL, "=");
        c = strtod(token, NULL);
        }

    discriminant=b*b-4*a*c;
    discriminant=sqrt(discriminant);
    if(discriminant==0)
        {
        root1=root2=(-1)*(b/(2*a));
        solution_exists=true;
        }
    if(discriminant>0)
        {
        root1=((-1)*b+discriminant)/(2*a);
        root2=((-1)*b-discriminant)/(2*a);
        solution_exists=true;
        }
    if(discriminant<0)
        {
        solution_exists=false;
        }
    roots.clear();  
    roots.push_back(root1);
    roots.push_back(root2);
    return roots;
    }

bool Taskfour::get_solution_exist()
    {
    return solution_exists;
    }