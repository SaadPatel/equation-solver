// 

#include "Task5.hpp"
#include "string.h"
#include <iostream>
using namespace std;
#include <stdio.h>
#include <stdlib.h>
#include<bits/stdc++.h>
#include <iomanip>

string Task5::change(string seq1, int x1, int y1)
{
  string temp;
  if(x1-y1>2)
  {
    temp=seq1.substr(y1+1,x1-y1-1);
  }
  else if(seq1[y1+1]=='+')
    temp="1";
  else if(seq1[y1+1]=='-')
    temp="-1";
  return temp;
}

double Task5::coeff(string seq1, int x1, int y1, int z1)
{
  double a1;
  string temp;
  if(x1==-1)
  { //no x
    a1=0;
  }
  else if(x1==0)
  {
    a1=1;
  }
  else if(y1==-1 && z1==-1)
  {//ax=d
    temp=seq1.substr(0,x1);
    a1=stof(temp);
  }
  else if(y1==-1)
  {//ax+bz=9
    if(x1<z1)
      temp=seq1.substr(0,x1);
    else
      temp=change(seq1,x1,z1);
    a1=stof(temp);
  }
  else if(z1==-1)
  {//ax+by=1
    if(x1<y1)
      temp=seq1.substr(0,x1);
    else
      temp=change(seq1,x1,y1);
    a1=stof(temp);
  }
  else
  {//ax+by+cz=1
    if(x1<y1 && x1<z1)
      temp=seq1.substr(0,x1);
    else if(y1<x1 && x1<z1)
      temp=change(seq1,x1,y1);
    else if(z1<x1 && x1<y1)
      temp=change(seq1,x1,z1);
    else
    {
      if(y1<z1)
        temp=change(seq1,x1,z1);
      else
        temp=change(seq1,x1,y1);
    }
    a1=stof(temp);
  }
  return a1;
}

void Task5::proper(string seq1, double &a1, double &b1, double &c1)
{
  int x1,y1,z1;
  x1=seq1.find('x');
  y1=seq1.find('y');
  z1=seq1.find('z');
  a1=coeff(seq1,x1,y1,z1);  //finding coeff of x1
  b1=coeff(seq1,y1,x1,z1);
  c1=coeff(seq1,z1,y1,x1);
}
double Task5::determinant(double a1,double b1,double c1,double a2,double b2,double c2,double a3,double b3,double c3)
{
  double s=a1*(b2*c3-b3*c2)-b1*(a2*c3-a3*c2)+c1*(a2*b3-a3*b2);
  return s;
}
string Task5::answer(double a1,double b1,double c1,double a2,double b2,double c2,double a3,double b3, double c3, double d1, double d2, double d3, double &x, double &y, double &z)
{
  double d,dx,dy,dz;
  string t;
  char cx[100],cy[100],cz[100];
  d=determinant(a1,a2,a3,b1,b2,b3,c1,c2,c3);
  dx=determinant(d1,d2,d3,b1,b2,b3,c1,c2,c3);
  dy=determinant(a1,a2,a3,d1,d2,d3,c1,c2,c3);
  dz=determinant(a1,a2,a3,b1,b2,b3,d1,d2,d3);
  if(d==0 && dx==0 && dy==0 && dz==0)
  {
    t= "1";//"Non-unique solution";
  }
  else if(d==0 && (dx!=0 || dy!=0 || dz!=0))
  {
    t="0";//"No solution";
  }
  else
  {
    x=dx/d;
    y=dy/d;
    z=dz/d;
    sprintf(cx,"%.4f",x);
    string sx(cx);
    sprintf(cy,"%.4f",y);
    string sy(cy);
    sprintf(cz,"%.4f",z);
    string sz(cz);
    t="x=";
    t.append(sx);
    t.append(";y=");
    t.append(sy);
    t.append(";z=");
    t.append(sz);
  }
  return t;
}
bool Task5::check(string e)
{
  /*char seps[]=";";
  char *eq1,*eq2,*eq3;
  eq1=strtok(&e[0],seps);
  eq2=strtok(NULL,seps);
  eq3=strtok(NULL,seps);
  string seq1(eq1);
  string seq2(eq2);
  string seq3(eq3);
  int ch1=seq1.find('x');
  int ch2=seq2.find('y');
  int ch3=seq3.find('z');*/
  int count=0;
  for(int i=0;i<e.length();i++)
  {
    if(e[i]==';')
      count++;
  }
  if(count==2)
  {
    //it is Task5
    return true;
  }
  else
  {
    return false;
  }
}
string Task5::output()
{
  vector<double> final=solve();
  double x,y,z;
  string t;
  if(final.size()==1)
  {
    if(final[0]==0)
      t="No solution";
    else
      t="Non-unique solution";
  }
  else
  {
    x=final[0];
    y=final[1];
    z=final[2];
  
    char varx[10];
    sprintf(varx,"%.4lf",x);
    string sx(varx);
    sprintf(varx,"%.4lf",y);
    string sy(varx);
    sprintf(varx,"%.4lf",z);
    string sz(varx);
    

    t="x=";
    t.append(sx);
    t.append(";y=");
    t.append(sy);
    t.append(";z=");
    t.append(sz);
  }
  return t;
}
vector <double> Task5::solve()
{
  //string equation
  char seps[]=";";
  char *eq1,*eq2,*eq3;
  int op1,op2,op3;
  eq1=strtok(&equation[0],seps);
  eq2=strtok(NULL,seps);
  eq3=strtok(NULL,seps);
  string seq1(eq1);
  string seq2(eq2);
  string seq3(eq3);
  double a1,a2,a3,b1,b2,b3,c1,c2,c3,d1,d2,d3,x,y,z;
  proper(seq1,a1,b1,c1);
  proper(seq2,a2,b2,c2);
  proper(seq3,a3,b3,c3);
  op1=seq1.find('=');
  int w1=seq1.length();
  op2=seq2.find('=');
  int w2=seq2.length();
  op3=seq3.find('=');
  int w3=seq3.length();
  d1=stof(seq1.substr(op1+1,w1-op1));
  d2=stof(seq2.substr(op2+1,w2-op2));
  d3=stof(seq3.substr(op3+1,w3-op3));
  vector<double> final;
  string ans=answer(a1,b1,c1,a2,b2,c2,a3,b3,c3,d1,d2,d3,x,y,z);
  if(ans=="0")
  {
    final.push_back(0);
  }
  else if(ans=="1")
  {
    final.push_back(1);
  }
  else
  {
    final.push_back(x);
    final.push_back(y);
    final.push_back(z);
  }
  return final;
}
