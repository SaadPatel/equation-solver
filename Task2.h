
#ifndef TASK2_H
#define TASK2_H
#include<vector>
#include "TaskTemplate.h"
#include <iostream>                
using namespace std;
class Task2 : public TaskInterface
{
   public:
    vector<double> gets(double X,double Y,double P,double x,double y,double p);
    string sustring(string s,int p,int d);
    vector<int> pos(string s);
    int getd(vector<int> d);
    vector<string>  san(int d,string s,vector<int> P);
     bool check(string e);
     vector<double> solve();
     string output() ;
     int SE;
};

#endif


