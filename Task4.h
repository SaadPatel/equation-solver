#ifndef TASKFOUR
#define TASKFOUR
#include"TaskTemplate.h"

#include<iostream>
#include <string>
using namespace std;
class Taskfour:public TaskInterface
{
    int a,b,c;
    bool  solution_exists;
    vector <double> roots;
    public:
    bool get_solution_exist();
    bool check(string e) ;// return true if the string e is your task, else return false 
    string output(); // return a string containing your results
    vector <double> solve() ; //function to solve the equation in the data member string
};
#endif