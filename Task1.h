#ifndef TASK1_H
#define TASK1_H
#include<bits/stdc++.h>
using namespace std;
#include"TaskTemplate.h"



class Task1:public TaskInterface{
    public:


        vector <double> solve(); //function to solve the equation in the data member string
        bool check(string e) ;// return true if the string e is your task, else return false 
        string output(); // return a string containing your results
    private:
        string ans;
        
};

#endif