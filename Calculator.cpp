#include "Calculator.h"

Calculator::Calculator()
{
    tasks.push_back(make_unique<Task1>());
    tasks.push_back(make_unique<Task2>());
    tasks.push_back(make_unique<Task3>());
    tasks.push_back(make_unique<Taskfour>());
    tasks.push_back(make_unique<Task5>());
    tasks.push_back(make_unique<Task6>());

    

}

void Calculator::calculate(string e)
{
    e = e.substr(1,e.size() - 1);           //remove  ""
    bool flag = true;

    for(auto & i: tasks)
    {
        if(i->check(e))
        {
            i->setEquation(e);
            cout<<i->output()<<endl;
            flag = false;
        }
    }

    if(flag)
    {
        cout<<"Invalid Input"<<endl;
    }
}