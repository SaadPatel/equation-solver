#include "TaskTemplate.h"
#ifndef TASK5
#define TASK5
class Task5:public TaskInterface
{
public:
  bool check(string e);
  string output();
  vector <double> solve();
  string change(string seq1, int x1, int y1);
  double coeff(string seq1, int x1, int y1, int z1);
  void proper(string seq1, double &a1, double &b1, double &c1);
  double determinant(double a1,double b1,double c1,double a2,double b2,double c2,double a3,double b3,double c3);
  string answer(double a1,double b1,double c1,double a2,double b2,double c2,double a3,double b3, double c3, double d1, double d2, double d3, double &X, double &y, double &z);

};
#endif
